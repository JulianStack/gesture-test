package onix.gesturetests.Asynctasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import onix.gesturetests.Models.ObjetoImagen;

/**
 * Created by JulianStack on 01/07/2016.
 */
public class GetJsondata extends AsyncTask<String, String, JSONObject> {

    HttpURLConnection connection;
    JSONObject jsonObject;
    Context mContext;
    JsonInterface mInterface;
    ArrayList<ObjetoImagen> Lista = new ArrayList<>();


    public GetJsondata(JsonInterface minterface,  Context context) {
        this.mContext = context;
        this.mInterface = minterface;
    }

    public interface JsonInterface {
        void call( ArrayList<ObjetoImagen> lista);
    }

    @Override
    protected JSONObject doInBackground(String... args) {
        try {
            URL url;
            HttpURLConnection connection = null;

            //Create connection
            url = new URL("http://23.251.149.115/db/prueba_android.json");
            connection = (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("POST");

            connection.setUseCaches(false);
            connection.setDoInput(true);
            //connection.setDoOutput(true);
            /*

            List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("zz1", Constants.AppId));


            //Send request
            DataOutputStream wr = new DataOutputStream (connection.getOutputStream ());
            wr.writeBytes (getQuery(params));
            wr.flush ();
            wr.close ();
            */

            //Get Response
            String aux = "";
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuffer response = new StringBuffer();
            while((line = rd.readLine()) != null) {
                response.append(line);
                aux += line;
            }


            JSONArray array = new JSONArray(aux);
            for(int i= 0; i<array.length(); i++){
                JSONObject js = array.getJSONObject(i);
                Lista.add(new ObjetoImagen(js.getString("title"),js.getString("imageUrl"),js.getString("letter"),null));
            }


            rd.close();
            return jsonObject;

        } catch (Exception e) {

            cancel(true);
            e.printStackTrace();
            return null;

        } finally {

            if(connection != null) {
                connection.disconnect();
            }
        }
    }

    @Override
    protected void onPostExecute(JSONObject result) {
        mInterface.call(Lista);
    }

    @Override
    protected void onCancelled(JSONObject result){
        System.out.println("ON CANCELLED (args)!");

        Log.d("JSON CANCELLED", "CANCELLED");
    }
}
