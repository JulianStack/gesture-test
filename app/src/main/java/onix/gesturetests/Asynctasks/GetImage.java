package onix.gesturetests.Asynctasks;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import java.io.InputStream;

/**
 * Created by JulianStack on 01/07/2016.
 */
public class GetImage extends AsyncTask<String, Void, Bitmap> {

    String Url;
    int Pos;
    // ArrayList<ImageItem> Lista2;
    Bitmap mIcon11 = null;

    public GetImage(MyBitmapCallback callback,String url,int pos) {
        this.mRespCallback = callback;
        this.Url = url;
        this.Pos = pos;
    }


    private MyBitmapCallback mRespCallback;

    public interface MyBitmapCallback {
        void call(Bitmap bitmap, int Pos);
    }

    protected Bitmap doInBackground(String... urls) {
        try {
            Log.d("bitmap", "trying");
            InputStream in = new java.net.URL(Url).openStream();
            mIcon11 = BitmapFactory.decodeStream(in);
        } catch (Exception e) {
            this.cancel(true);
            Log.e("Error in bitmap", e.getMessage());
            Log.d("bitmap", "error");
            e.printStackTrace();
        }
        return mIcon11;
    }

    protected void onPostExecute(Bitmap result) {
        mRespCallback.call(mIcon11,Pos);
        Log.d("bitmap", "retieved");
    }
}
