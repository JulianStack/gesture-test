package onix.gesturetests.Models;

import android.graphics.Bitmap;

/**
 * Created by JulianStack on 30/06/2016.
 */
public class ObjetoImagen {

    String Title;
    String ImageUrl;
    String Letter;
    Bitmap Image;

    public ObjetoImagen(String title, String imageUrl, String letter, Bitmap image) {
        Title = title;
        ImageUrl = imageUrl;
        Letter = letter;
        Image = image;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        ImageUrl = imageUrl;
    }

    public String getLetter() {
        return Letter;
    }

    public void setLetter(String letter) {
        Letter = letter;
    }

    public Bitmap getImage() {
        return Image;
    }

    public void setImage(Bitmap image) {
        Image = image;
    }
}
