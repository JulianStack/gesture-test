package onix.gesturetests.Activities;

import android.app.Activity;
import android.gesture.Gesture;
import android.gesture.GestureLibraries;
import android.gesture.GestureLibrary;
import android.gesture.GestureOverlayView;
import android.gesture.GestureOverlayView.OnGesturePerformedListener;
import android.gesture.Prediction;
import android.graphics.Bitmap;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import onix.gesturetests.Asynctasks.GetImage;
import onix.gesturetests.Asynctasks.GetJsondata;
import onix.gesturetests.Models.ObjetoImagen;
import onix.gesturetests.R;

/**
 * Created by JulianStack on 01/07/2016.
 */
public class GestosActivity extends Activity implements OnGesturePerformedListener {

    private GestureLibrary gestureLib;
    ArrayList<ObjetoImagen> Lista = new ArrayList<>();
    ProgressBar progress;
    ImageView Picture;
    TextView Text;


    public static final String TAG = "GestureActivity";
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GestureOverlayView gestureOverlayView = new GestureOverlayView(this);
        View inflate = getLayoutInflater().inflate(R.layout.activity_gestos, null);
        gestureOverlayView.addView(inflate);
        gestureOverlayView.addOnGesturePerformedListener(this);
        gestureOverlayView.setGestureVisible(false);
        // load custom gestures
        gestureLib = GestureLibraries.fromRawResource(this, R.raw.gestures);
        gestureLib.load();
        if (!gestureLib.load()) { //gestureLib = GestureLibraries.fromRawResource(this, R.raw.gestures);
            Log.d(TAG,"Could not load library");
            finish();
        }

        setContentView(gestureOverlayView);

        progress = (ProgressBar)findViewById(R.id.progressBar);
        Picture = (ImageView)findViewById(R.id.imageView);
        Text = (TextView)findViewById(R.id.textView);

        progress.setVisibility(View.GONE);

        // get Json Info
        getInfo("");
    }

    private void getInfo(final String name) {
        new GetJsondata(new GetJsondata.JsonInterface() {
            @Override
            public void call(ArrayList<ObjetoImagen> lista) {
                Lista.addAll(lista);
                Toast.makeText(GestosActivity.this, "Data Retrieved", Toast.LENGTH_SHORT).show();
                if(!name.equals("")){
                    LoadData(name);
                }
            }
        },this).execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_gestos, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onGesturePerformed(GestureOverlayView overlay, Gesture gesture){
        ArrayList<Prediction> predictions = gestureLib.recognize(gesture);

        String Name = getName(predictions);
        Toast.makeText(GestosActivity.this, Name, Toast.LENGTH_SHORT).show();
        if(Lista.size()==0){
            getInfo(Name);
        }else{
            LoadData(Name);
        }


    }

    public String getName(ArrayList<Prediction> predictions){
        double max =  predictions.get(0).score;
        int Pos = 0;

        for (int i = 1; i < predictions.size(); i++) {
            if (predictions.get(i).score > max) {
                max = predictions.get(i).score;
                Pos = i;
            }
        }
        return predictions.get(Pos).name;
    }


    public void LoadData(String name){

        Picture.setImageBitmap(null);
        Text.setText(null);

        progress.setVisibility(View.VISIBLE);
        int index = getIndexwithName(name);

        if(Lista.get(index).getImage()== null){
            new GetImage(new GetImage.MyBitmapCallback() {
                @Override
                public void call(Bitmap bitmap, int Pos) {
                    Lista.get(Pos).setImage(bitmap);
                    ShowData(Pos);
                }
            },Lista.get(index).getImageUrl(),index).execute();
        }else{
            ShowData(index);
        }

    }

    private void ShowData(int index) {
        progress.setVisibility(View.GONE);
        Picture.setImageBitmap(Lista.get(index).getImage());
        Text.setText(Lista.get(index).getTitle());
    }


    public int getIndexwithName(String name){
        int a = 0;
        for(int i = 0; i<Lista.size(); i++){
            if(Lista.get(i).getLetter().equals(name)){
                a=i;
                return i;
            }
        }
        return a;
    }
}
